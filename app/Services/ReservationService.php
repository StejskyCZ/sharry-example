<?php
/**
 * Created by PhpStorm.
 * User: jakub
 * Date: 14.12.2017
 * Time: 0:26
 */

namespace App\Services;


use App\Interfaces\ReservableInterface;
use App\User;

class ReservationService
{
    /** @var  ReservableInterface */
    private $item;

    /** @var User */
    private $user;

    /**
     * ReservationService constructor.
     * @param ReservableInterface $reservableItem
     * @param User $user
     */
    public function __construct(ReservableInterface $reservableItem, User $user)
    {
        $this->item = $reservableItem;
        $this->user = $user;
    }

    public function reserve($start, $end)
    {
        return $this->item->reserve($start, $end, $this->user);
    }
}