<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reservation extends Model
{
    protected $fillable = ['start', 'end', 'user_id'];

    public function reservable()
    {
        return $this->morphTo();
    }
}
