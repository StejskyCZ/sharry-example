<?php

namespace App;

use App\Interfaces\ReservableInterface;
use App\Traits\ReservableTrait;
use Illuminate\Database\Eloquent\Model;

class ParkingLot extends Model implements ReservableInterface
{
    use ReservableTrait;

}
