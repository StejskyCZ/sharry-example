<?php

namespace App\Traits;

use App\Reservation;
use App\User;

trait ReservableTrait {
    public function reservations()
    {
        return $this->morphMany(Reservation::class, 'reservable');
    }

    public function checkAvailability($start, $end) : bool
    {
        $reservations = $this->reservations()
            ->whereBetween('start', [$start, $end])
            ->orWhereBetween('end', [$start, $end])->get();

        return $reservations->isEmpty() ? true : false;
    }

    public function reserve($start, $end, User $user)
    {
        if ($this->checkAvailability($start, $end)) {
            return $this->reservations()->create(['start' => $start, 'end' => $end, 'user_id' => $user->id]);
        }

        return false;
    }
}