# Ukázkový kód pro SHARRY #

Dobrý den Vladimíre,
vzhledem k tomu, že jsem nenašel žádný aspoň trochu zajímavější kód z mých projektů (většina jsou obyčejné weby a k opravdu pěkným kódům z minulosti již nemám přístup), 
rozhodl jsem se napsat něco vlastního.
Jedná se o ukázku toho, jak bych v začátku vyvíjel appku, sloužící k obsluze inteligentních budov.
Je to samozřejmě velice zjednodušená (naivní) implementace, jde mi čistě o ukázku mého psaní objektového kódu a lépe se mi pracuje s nějakým konkrétním zadáním, než vyloženě abstraktníma třídama.


### Základní informace ###

* Projekt je psaný na Laravelu, přemýšlel jsem i o čistém PHP nebo Symfony, ale řekl jsem si, že ukázku udělám v tom, v čem jsem nejvíc kovaný.
* Je to celé psané jako Test Driven Development (PHPUnit), jedná se čistě o backend.

### Implementované funkce ###

* Ověření přístupové role do budovy pro uživatele.
* Rezervace konferenčních místností, parkovacích míst a kol.

### Co se mi povedlo ###

* Polymorphní návrh rezervací, jak v DB, tak v kódu, tzn. velice snadná znovupoužitelnost na další modely pouze pomocí interface a traitu.
* Používání interface v typehintu místo konkrétních tříd (ReservationService).

### Co bych dělal v reálném projektu jinak ###
* Skoro vše :-) počínaje napsáním API, přes které by šla všechna data, zanesením serviců do DI containeru, kam patří, používáním eventů a listenerů v případě úspěšné registrace atd.

Tento "projekt" jsem napsal za cca 3 hodiny, abyste viděl aspoň v základu způsob mého programátorského uvažování.
Doufám, že to na ukázku bude stačit :-)

Jakub
