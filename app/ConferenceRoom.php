<?php

namespace App;

use App\Interfaces\ReservableInterface;
use App\Traits\ReservableTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class ConferenceRoom extends Model implements ReservableInterface
{
    use ReservableTrait;

    protected $fillable = ['building_id'];

    public function building() : BelongsTo
    {
        return $this->belongsTo(Building::class);
    }


}
