<?php

namespace App\Interfaces;

use App\User;

interface ReservableInterface
{
    public function reserve($start, $end, User $user);
    public function checkAvailability($start, $end);
}