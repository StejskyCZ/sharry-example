<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Query\Builder;

class Role extends Model
{
    protected $fillable = ['name', 'building_id'];

    public function building() : BelongsTo
    {
        return $this->belongsTo(Building::class);
    }

    public function user() : BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function scopeBuilding($query, Building $building) : Builder
    {
        return $query->where('building_id', $building);
    }
}
