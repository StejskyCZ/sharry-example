<?php

namespace Tests\Feature;

use App\Bike;
use App\Building;
use App\ConferenceRoom;
use App\ParkingLot;
use App\Reservation;
use App\Services\BuildingService;
use App\Services\ReservationService;
use App\User;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserTest extends TestCase
{
    use DatabaseMigrations;

    /** @var  Building */
    private $building;

    /** @var  User */
    private $user;

    public function setUp()
    {
        parent::setUp();
        $this->building = factory(Building::class)->create();
        $this->user = factory(User::class)->create();
        $this->actingAs($this->user);
    }

    /** @test */
    public function user_can_access_building()
    {
        $buildingService = new BuildingService($this->building);
        // User cannot access building without adequate role
        $this->assertFalse($buildingService->accessBuilding($this->user));

        $this->user->roles()->create(['name' => 'access', 'building_id' => $this->building->id]);
        // User can now access the building
        $this->assertTrue($buildingService->accessBuilding($this->user));
    }

    /** @test */
    public function user_can_book_a_conference_room()
    {
        // Create a conference room and dates
        $conferenceRoom = factory(ConferenceRoom::class)->create(['building_id' => $this->building->id]);
        $dates = $this->getStartAndEndDates();

        // Successfully booked a Conference Room
        $reservationService = new ReservationService($conferenceRoom, $this->user);
        $this->assertInstanceOf(Reservation::class, $reservationService->reserve($dates['start'], $dates['end']));

        // Room is already booked, return false
        $this->assertFalse($reservationService->reserve($dates['start'], $dates['end']));
    }

    /** @test */
    public function user_can_book_a_parking_lot()
    {
        $parkingLot = factory(ParkingLot::class)->create();
        $reservationService = new ReservationService($parkingLot, $this->user);

        $dates = $this->getStartAndEndDates();

        // Successfully created a booking for parking lot
        $this->assertInstanceOf(Reservation::class, $reservationService->reserve($dates['start'], $dates['end']));

        // Parking lot is already booked, return false
        $this->assertFalse($reservationService->reserve($dates['start'], $dates['end']));
    }

    /** @test */
    public function user_can_book_a_bike()
    {
        $bike = factory(Bike::class)->create();
        $reservationService = new ReservationService($bike, $this->user);

        $dates = $this->getStartAndEndDates();

        // Successfully booked a bike. Let's get healthy!
        $this->assertInstanceOf(Reservation::class, $reservationService->reserve($dates['start'], $dates['end']));

        // No more bikes left, you have to walk now :-(
        $this->assertFalse($reservationService->reserve($dates['start'], $dates['end']));
    }

    private function getStartAndEndDates()
    {
        $start = new Carbon();
        $end = clone $start;
        $end->addHour();

        return [
            'start' => $start,
            'end' => $end
        ];
    }
}
