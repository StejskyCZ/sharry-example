<?php namespace App\Services;

use App\Building;
use App\ReservableInterface;
use App\User;

class BuildingService
{
    /** @var Building */
    private $building;

    public function __construct(Building $building)
    {
        $this->building = $building;
    }

    public function accessBuilding(User $user) : bool
    {
        return $user->hasRole('access') ? true : false;
    }
}